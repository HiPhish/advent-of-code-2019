#####################
 Advent of Code 2019
#####################

This repo contains my solutions to the `Advent of Code`_ challenges for
`2019`_. Each solution will be written using whatever language and techniques
seem the most appropriate for the task at hand to me. This is my first time
participating, so no fancy tricks this year.


.. ----------------------------------------------------------------------------
.. _Advent of Code: https://adventofcode.com/
.. _2019: https://adventofcode.com/2019/
