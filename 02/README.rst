###########################
 Day 2: 1202 Program Alarm
###########################

For this challenge I have picked C because I will be building a VM and
manipulating raw memory, and C is really good at it. Unfortunately, this also
means that the parser which reads to the program which the VM will execute is
also written in C. When handling individual bytes care must be taken to deal
with things like trailing line endings (or lack thereof).

Once the input text was converted to integers the rest was trivial: Read the
opcode (an integer) and the current instruction pointer address, look up the
instruction (a function pointer) which it corresponds to, and execute it. Keep
doing this until the VM halts eventually.

What comes next depends on the problem at hand. For the first part I just had
to look up the memory value at address `0`. For the second part I first had to
patch the memory, run the VM, check the memory at address `0` and keep
repeating these steps until the right patch was found.
