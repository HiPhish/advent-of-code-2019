#include <stdio.h>
#include <stdlib.h>
#include "vm.h"


/** Return the instruction which corresponds to the current opcode of the
 * virtual machine.
 *
 * @param opcode  The opcode to execute
 * @return Function pointer to an instruction
 */
void (*get_instruction(struct vm *vm))(struct vm *);

/* Virtual machine instructions */
void ADD (struct vm *vm);
void MULT(struct vm *vm);
void HALT(struct vm *vm);
void FAIL(struct vm *vm);


int read_int(int *i, FILE *in) {
	int success = 0, c, result = 0;

	/* Once we hit the first digit we mark the reading as successful. If we
	 * have not hit any digits yet we keep reading (skips over whitespace,
	 * commas and other non-digits). If we hit EOF we abort for sure. If we hit
	 * a non-digit, but we have already hit a digit we finish (usually means we
	 * hit a comma or EOF after a number).*/
loop:
	c = getc(in);
	if ('0' <= c && c <= '9') {
		success = 1;
		result *= 10;
		result += c - '0';
		goto loop;
	} else if (c == EOF) {
		goto end;	
	} else if (success) {
		goto end;
	}
	goto loop;	

end:
	*i = result;
	return success;
}

size_t read_vm(struct vm *vm, FILE *in) {
	const size_t increment = 50;  /* A guess */
	size_t size = 0, max_size = increment;
	int *memory = NULL;

	if (!(memory = calloc(max_size, sizeof *memory))) {
		goto fail;
	}

	int datum;
	while (read_int(&datum, in)) {
		if (size == max_size) {
			/* Re-allocate memory to a larger array */
			max_size += increment;
			memory = reallocarray(memory, max_size, sizeof *memory);
			if (!*memory) {
				goto fail;
			}
		}
		memory[size++] = datum;
	}

	vm->memory = memory;
	vm->size = size;
	vm->ic = 0;
	vm->state = VM_NEW;
	return size;

fail:
	free(memory);
	return 0;
}

void run_vm(struct vm *vm) {
	vm->state = VM_RUNNING;
	while (vm->state == VM_RUNNING) {
		void (*instruction)(struct vm *) = get_instruction(vm);
		instruction(vm);
	}
}

void (*get_instruction(struct vm *vm))(struct vm *) {
	void (*instruction)(struct vm *) = NULL;
	switch (vm->memory[vm->ic]) {
		case  1: instruction = ADD ; break;
		case  2: instruction = MULT; break;
		case 99: instruction = HALT; break;
		default: instruction = FAIL; break;
	}
	return instruction;
}

void ADD(struct vm *vm) {
	size_t ic = vm->ic;
	int *memory = vm->memory;
	memory[memory[ic + 3]] = memory[memory[ic + 1]] + memory[memory[ic + 2]];
	vm->ic += 4;
}

void MULT(struct vm *vm) {
	size_t ic = vm->ic;
	int *memory = vm->memory;
	memory[memory[ic + 3]] = memory[memory[ic + 1]] * memory[memory[ic + 2]];
	vm->ic += 4;
}

void HALT(struct vm *vm) {
	vm->state = VM_TERMINATED;
}

void FAIL(struct vm *vm) {
	fprintf(stderr, "Error: unknown opcode %d\n", vm->memory[vm->ic]);
	vm->state = VM_FAILED;
}
