#ifndef VM_H
#define VM_H

#include <stdio.h>

/** Possible states a virtual machine can be in */
enum vm_state {
	VM_NEW,
	VM_RUNNING,
	VM_TERMINATED,
	VM_FAILED,
};

/** Structure representing a virtual machine instance. */
struct vm {
	int *memory; /* Memory of the VM */
	size_t size; /* Length of the memory */
	size_t ic;   /* Instruction counter */
	enum vm_state state;
};

/** Initialise a new VM with memory loaded from a test file
 *
 * The `memory` does not need to be allocated, it will be allocated and
 * initialised if the program could have been successfully loaded. The
 * recipient is responsible for the newly allocated memory. As a side effect
 * the position of the input file will be advanced.
 *
 * @param vm  Pointer to the VM to initialise
 * @param in  File to read the program from
 *
 * @return Size of the memory in integers, or zero if an error occurred.
 */
size_t read_vm(struct vm *vm, FILE *in);

/** Run the VM by executing its in-memory program.
 *
 * Execution begins wherever the instruction counter currently is.
 *
 * @param vm  The VM to run
 */
void run_vm(struct vm *vm);

#endif  /* ifndef VM_H */
