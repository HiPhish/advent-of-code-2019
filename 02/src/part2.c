#include <stdlib.h>
#include <string.h>
#include "vm.h"

int main(int argc, char *argv[]) {
	struct vm vm = {.memory = NULL, .size = 0, .ic = 0};
	int *program = NULL;
	int verb, noun;

	if (!read_vm(&vm, stdin) || !(program = calloc(sizeof *vm.memory, vm.size))) {
		fputs("Error reading program from file.", stderr);
		return 1;
	}

	/* Keep a copy of the original program, we will be swapping out the memory
	 * of the VM for other memory sequences and later restoring the original
	 * one */
	memcpy(program, vm.memory, vm.size * sizeof *vm.memory);

	for (noun = 0; noun <= 99; ++noun) {
		for (verb = 0; verb <= 99; ++verb) {
			vm.memory[1] = noun;
			vm.memory[2] = verb;
			
			run_vm(&vm);

			if (vm.memory[0] == 19690720) {
				goto success;
			}

			/* Reset the VM to its original state */
			memcpy(vm.memory, program, vm.size * sizeof *vm.memory);
			vm.ic = 0;
			vm.state = VM_NEW;
		}
	}

	puts("No input combination found");
	return 1;

success:
	printf("Input combination %d and %d found\n", verb, noun);
	printf("100 * noun + verb = %d\n", 100 * noun + verb);
	return 0;
}
