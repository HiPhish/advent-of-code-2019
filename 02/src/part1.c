#include <stdio.h>
#include "vm.h"

int main(int argc, char *argv[]) {
	struct vm vm = {.memory = NULL, .size = 0, .ic = 0};

	if (!read_vm(&vm, stdin)) {
		fputs("Error reading program from file.", stderr);
		return 1;
	}
	
	/* Patch the program first as instructed in the problem */
	vm.memory[1] = 12;
	vm.memory[2] =  2;

	run_vm(&vm);

	for (size_t i = 0; i < vm.size; ++i) {
		printf("%d%s", vm.memory[i], i == vm.size - 1 ? "\n" : ",");
	}

	return 0;
}
