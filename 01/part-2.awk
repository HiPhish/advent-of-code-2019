# https://adventofcode.com/2019/day/1#part2

function max(x, y) { return x > y ? x : y }

# Use _fuel as a local variable
function mass_to_fuel(mass,  _fuel) {
	while (mass > 0) {
		mass = max(int(mass / 3) - 2, 0)
		_fuel += mass
	}
	return _fuel
}

{ total_fuel += mass_to_fuel($1) }

END { print total_fuel }
