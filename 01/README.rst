###########################################
 Day 1: The Tyranny of the Rocket Equation
###########################################

I have decided to use Awk because it automatically takes care of parsing text
into numbers and looping over the lines. All I had to do was specify was how to
process the data. In addition, my `Awk-ward.nvim`_ plugin for Neovim_ allows me
to see the result live as I am writing my code, letting me develop the solution
in an iterative manner.

The first part is straight-forward and could have been written on one line if
space mattered. I have assumed that the input is correctly formatted, meaning
that every line contains a well-formed integer number. A real Awk script would
be required to specify a correct pattern and either ignore malformed lines or
issue an error message.

The only pitfall with Awk I encountered was that all variables are global by
default:

.. code-block:: awk

   function mass_to_fuel(mass) {
	   while (mass > 0) {
		   mass = max(int(mass / 3) - 2, 0)
		   fuel += mass
	   }
	   return fuel
   }

The `fuel` variable will keep accumulating results from previous call of the
function. The only way of declaring a local variable in Awk is to specify it as
a function parameter:

.. code-block:: awk

   function mass_to_fuel(mass,  _fuel) {
	   while (mass > 0) {
		   mass = max(int(mass / 3) - 2, 0)
		   _fuel += mass
	   }
	   return _fuel
   }


.. ----------------------------------------------------------------------------
.. _Awk-ward.nim: https://gitlab.com/HiPhish/awk-ward.nvim
.. _Neovim: https://neovim.io/
