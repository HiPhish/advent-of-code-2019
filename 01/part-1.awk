# This formula may result in negative fuel if the mass is less than six, but
# since the task does not say anything about negative fuel I will just assume
# that elf magic works like this.

{ total_fuel += int($0 / 3) - 2 }

END { print total_fuel }
