local Wire     = require 'wire'
local Vector2  = require 'vector2'
local geometry = require 'geometry'
local manhattan = geometry.manhattan


local wires = {}  -- All the wires in the puzzle
for line in io.lines() do
	wires[#wires + 1] = Wire.read(line)
end

-- Iterate over the wire segments, looking for intersections. When an
-- intersection is found and its distance from the origin is smaller than the
-- previous one, store it. We ignore the origin, which is the trivial
-- intersection.

local nearest = nil
for _, segment1 in ipairs(wires[1]) do
	for _, segment2 in ipairs(wires[2]) do
		local intersection = geometry.intersection(segment1, segment2)
		if intersection and intersection:norm(manhattan) > 0 then
			if not nearest or intersection:norm(manhattan) < nearest:norm(manhattan) then
				nearest = intersection
			end
		end
	end
end

print(nearest:norm(manhattan))
