-- Various geometry functions go in this module


-- Compute the intersection point of two line segments. If the line segments
-- overlap the result will be the left-most bottom-most overlapping point, but
-- we will simply assume that the puzzle does not contain any overlapping line
-- segments and conveniently ignore that edge case.
-- 
-- The result is the intersection point and the two fractions along the line
-- segments where they meet.
local function intersection(s1, s2)
	local p, v = s1.start, s1.direction
	local q, w = s2.start, s2.direction

	-- Let p be the starting point of s1, and q the starting point of s2. Let v
	-- be the direction vector of s1, and w the direction vector of s2. The
	-- intersection point r can be expressed ad
	--
	--    r = p + t * v
	--    r = q + u * w
	--
	-- for some t, u ∈ [0, 1]. If the segments do not intersect, then there are
	-- no such t, u. See below for the definition of the two-dimensional cross
	-- product.
	--
	--                p + t * v = q + u * w
	-- <=>      (p + t * v) × w = (q + u * w) × w
	-- <=>    p × w + t * v × w = q × w + u * w × w
	-- <=>    p × w + t * v × w = q × w
	-- <=>            t * v × w = (q - p) × w
	-- <=>                    t = ((q - p) × w) / (v × w)
	--
	-- Conversely:
	--
	--                p + t * v = q + u * w
	-- <=>      (p + t * v) × v = (q + u * w) × v
	-- <=>                p × v = q × v + u * w × v
	-- <=>          (p - q) × v = u * w × v
	-- <=>                    u = ((p - q) × v) / (w × v)
	-- <=>                    u = ((q - p) × v) / (v × w)
	--
	-- https://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
	--
	-- There is something awfully familiar about this, but I cannot quite put
	-- my finger on it. It might have something to do with Clifford algebra or
	-- complex numbers; I would have to investigate the topic deeper.

	local function cross_product(v, w)
		return v.x * w.y - v.y * w.x
	end

	local a = cross_product(q - p, w)
	local b = cross_product(q - p, v)
	local c = cross_product(v, w)

	if c == 0 then
		return nil
	end

	local t = a / c
	local u = b / c

	-- It is important to check that both t and s are within range, otherwise
	-- we can get a point that lies on one line segment, but not the other.
	if 0 <= t and t <= 1 and 0 <= u and u <= 1 then
		return p + v * t, t , u
	end

	return nil
end

-- Manhattan-metric of two vectors
local function manhattan(v, w)
	return math.abs(v.x - w.x) + math.abs(v.y - w.y)
end


return {
	intersection = intersection,
	manhattan = manhattan,
}
