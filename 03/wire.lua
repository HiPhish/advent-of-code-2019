-- Wire representation as a sequence of segments. Each segment consists of a
-- starting point, a direction and a distance in this direction. The end point
-- of a segment needs to be computed. We keep track of the very last point of a
-- wire for convenience.

local Vector2 = require 'vector2'
local Wire = {}

-- Map symbolic directions to direction vectors
local directions = {
	R = Vector2.new( 1,  0),
	L = Vector2.new(-1,  0),
	U = Vector2.new( 0,  1),
	D = Vector2.new( 0, -1),
}

-- Extend the wire by appending a new segment, relative to the previous vertex
local function extend(self, direction)
	local segment = {
		start = self.last,
		direction = direction,
	}
	self.last = segment.start + segment.direction
	self[#self + 1] = segment
end

-- Wire representation as a sequence of segments
function Wire.new()
	return {
		last = Vector2.new(0, 0),
		extend = extend,
	}
end

-- Read a wire from a wire specification string
function Wire.read(specs)
	local w = Wire.new()

	for symbol, length in string.gmatch(specs, "([RLUD])(%d+)") do
		w:extend(directions[symbol] * tonumber(length))
	end

	return w
end


-------------------------------------------------------------------------------
return Wire
