local Wire     = require 'wire'
local Vector2  = require 'vector2'
local geometry = require 'geometry'
local manhattan = geometry.manhattan


local wires = {}  -- All the wires in the puzzle
for line in io.lines() do
	wires[#wires + 1] = Wire.read(line)
end


-- Part 2 is a bit messy, but I don't care at this point. Keep track of the
-- length of the wire by summing up the lengths of the segments so far. When we
-- find an intersection the length along the wire is the length of the segments
-- so far, plus the fraction along the last segment times the total length of
-- the last segment.
--
-- Example: O---O-----O---------X-----
--          If the X is the intersection, then its distance along the wire is
--             4 + 6 (15 * 2/3)
--          (length of the first two segments and two thirds of the last one)
--
-- Do the same thing for the other wire and add the results.

local nearest = nil
local wire1_length = 0
for _, s1 in ipairs(wires[1]) do
	local wire2_length = 0
	for _, s2 in ipairs(wires[2]) do
		local intersection, t, u = geometry.intersection(s1, s2)
		if intersection and intersection:norm(manhattan) > 0 then
			t = t * s1.direction:norm(manhattan)
			u = u * s2.direction:norm(manhattan)
			local steps = wire1_length + wire2_length + t + u
			if not nearest or steps < nearest then
				nearest = steps
			end
		end
		wire2_length = wire2_length + s2.direction:norm(manhattan)
	end
	wire1_length = wire1_length + s1.direction:norm(manhattan)
end

print(nearest)
