.. default-role:: code

######################
 Day 3: Crossed Wires
######################

A wire is a sequence of line segments where the first point of one segment is
last point of the previous segment. Lua tables are a natural fit because the
way tables work we can store any information we want in a table, and we can
enumerate over the items with numeric index.

.. code-block:: lua

   -- Loop over all segments
   for i, segment in ipairs(wire) do
       print(segment.start.x, segment.start.y)
   end

Since vertical line segments exist, we have to represent a line as a pair of a
starting point vector and a direction vector, rather than using the slop of the
line (vertical lines have no defined slope). The other end point is the sum of
the two vectors.

.. code-block:: lua
   -- Definition of a line segment
   segment = {start = Vector2(x, y), direction = Vector2(u, v)}
   end_point = segment.start + segment.direction

Lua meta-tables allow us to define vector addition on all tables created
through `Vector2.new`

.. code-block:: lua

   local mt = {}  -- The meta-table

   -- Add two vectors to create one new vector
   function mt.__add(v1, v2)
      return Vector2.new(v1.x + v2.x, v1.y + v2.y)
   end

   -- 2D Vector constructor
   function Vector2.new(x, y)
       local v = {
           x = x,
           y = y,
           distance = manhattan_distance
       }
       setmetatable(v, mt)
       return v
   end

Finding the intersections of two wires thus reduces to the problem of finding
the intersections of all the line segments. The representation of line segments
is general enough to handle segments of any length (not just integer length) at
any angle, so we can solve the problem using any general algorithm or formula.
