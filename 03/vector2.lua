local Vector2 = {}

local mt = {}

function mt.__add(v1, v2)
	local x = v1.x + v2.x
	local y = v1.y + v2.y
	return Vector2.new(x, y)
end

function mt.__mul(v, s)
	local x = v.x * s
	local y = v.y * s
	return Vector2.new(x, y)
end

function mt.__sub(v1, v2)
	return v1 + (v2 * -1)
end

function mt.__tostring(v)
	return string.format("(%d, %d)", v.x, v.y)
end

local function norm(self, metric)
	-- Use the Euclidean metric as the default
	local metric = metric or function (v, w)
		return math.sqrt((v.x - w.x)^2 + (v.y - w.y)^2)
	end
	return metric(self, Vector2.new(0, 0))
end

function Vector2.new(x, y)
	local v = {
		x = x,
		y = y,
		norm = norm
	}
	setmetatable(v, mt)
	return v
end


-------------------------------------------------------------------------------
return Vector2
